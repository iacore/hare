#!/usr/bin/env janet

(import path)

(defn is-dir-module [name]
  (cond
   # ignore dot files
   (string/has-prefix? "." name) false
   # ignore non-module directories
   (find |(= $ name) ["scripts" "cmd" "contrib" "docs"]) false

   (= ((os/stat name) :mode) :directory)))


(def *outdir* :haredoc_outdir)

(defn generate-doc [dirname outhtmlname]
  (def command ["haredoc" "-F" "html" (splice dirname)])
  (printf "%p -> %p" dirname outhtmlname)
  (printf "Executing: %p" command)
  (def fout (os/open (path/join (dyn *outdir*) outhtmlname) :cwt))
  (os/execute command :px {:out fout}))

(defn main [&]
  (setdyn *outdir* "scripts/docs/html")
  (os/cd (path/join (path/dirname (dyn *current-file*)) ".."))
  (def mods (->> (os/dir (os/cwd))
                (filter is-dir-module)))
  (generate-doc [] "index.html")
  (each modname mods
    (generate-doc [modname] (string modname ".html"))))

